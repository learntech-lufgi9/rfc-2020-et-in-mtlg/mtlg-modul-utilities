/**
 * @Date:   2019-04-09T11:20:16+02:00
 * @Last modified time: 2020-03-10T08:48:09+01:00
 */



/**
 * This namespace provides several functions that can be used to easily create UI elements.
 * @namespace uiElements
 * @memberof MTLG.utils
 */
var uiElements = (function(){

  /**
   * Creates a simple button in a new container and returns the container.
   * @param {object} properties A properties object that can specify all properties described below
   * @param {function} callback The function that is called when the button is clicked
   * @return {object} Container that contains the new button.
   * @property {string} font The font type to use
   * @property {string} text The text in the button
   * @property {number} sizeX The width of the button
   * @property {number} sizeY The height of the button
   * @property {string} textColor The color of the button text
   * @property {string} bgColor1 The fill color of the background
   * @property {string} bgColor2 The stroke color of the background
   * @property {number} radius The radius for a rounded rectangle
   * @example
   * //Create a button of size 100x70 that logs to the console when it is clicked
   * let prop = {font: 'Arial', text: "Click", sizeX: 100, sizeY : 70};
   * var button = MTLG.utils.uiElements.addButton(prop, function(){
   *   console.log("Button Clicked")
   * });
   * //Change attributes of the container
   * button.x = 50;
   * //Add container to stage or other container
   * myStage.addChild(button);
   * @memberof MTLG.utils.uiElements#
   */
  let addButton = function(properties, callback){
    var defaults = {
      font: "Arial",
      text: "",
      sizeX: 100,
      sizeY: 100,
      textColor: "black",
      bgColor1: "white",
      bgColor2: "black",
      radius: 0
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();
    let buttonText = MTLG.utils.gfx.getText(defaults.text, defaults.sizeY / 2+ 'px ' + defaults.font,defaults.textColor);
    let textMargin = 9 / 10; // Margin relative to sizeX
    buttonText.maxWidth = defaults.sizeX * textMargin;
    buttonText.x = defaults.sizeX / 2; //Since regX is half the size
    buttonText.y = defaults.sizeY / 2 + buttonText.getBounds().height/4;
    buttonText.regX = buttonText.getBounds().width/2;
    //buttonText.regY = buttonText.getBounds().height/2;
    buttonText.textBaseline = 'alphabetic';
    buttonText.name = "text";
    //TODO Text x size

    let buttonBg = MTLG.utils.gfx.getShape();
    buttonBg.graphics.beginStroke(defaults.bgColor2).beginFill(defaults.bgColor1).drawRoundRect(0, 0, defaults.sizeX, defaults.sizeY, defaults.radius);
    //buttonBg.regX = sizeX / 2;
    //buttonBg.regY = sizeY / 2;
    //buttonBg.x = sizeX / 2;
    buttonBg.y = 0; //sizeY / 2;
    buttonBg.name = "background";

    container.addChild(buttonBg);
    container.addChild(buttonText);
    container.regX = defaults.sizeX / 2;
    container.regY = defaults.sizeY / 2;
    container.on('click', callback);

    container.setBounds(0, 0, defaults.sizeX, defaults.sizeY);
    return container;
  }

  /**
   * Creates a simple checkbox in a new container and returns the container.
   * @param {object} properties A properties object that can specify all properties described below
   * @property {string} font The font that is used for the text next to the checkbox
   * @property {number} size The size of the checkbox (which is square)
   * @property {string} firstColor The inner color of the checkbox (visible when value is true)
   * @property {string} secondColor The outer color of the checkbox
   * @property {boolean} defaultVal Initial value of the checkbox
   * @property {string} description The description text that is printed next to the checkbox
   * @param {function} clickOnFunction The function that is called when the checkbox is clicked
   * @return {object} The container with the new checkbox
   * @example
   * //Create a checkbox of size 40x40 that logs its state to the console when it is clicked
   * let checkProps = {font: "Impact", size: 40, firstColor: "red", secondColor: "grey", description: "Checkbox Test."};
   * var newCheckbox = MTLG.utils.uiElements.addCheckbox(checkProps, function(value) {
   *   console.log("Checkbox new value:" + value);
   * });
   * //Change properties of container
   * newCheckbox.x = 50;
   * newCheckbox.y = 400;
   * //Add the container to the stage or another container
   * stage.addChild(newCheckbox);
   * @memberof MTLG.utils.uiElements#
   */
  let addCheckbox = function(properties, clickOnFunction){
    let defaults = {
      font: "Arial",
      size: 50,
      firstColor: "red",
      secondColor: "black",
      defaultVal: false,
      description: "",
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();

    //description
    let descriptionText = MTLG.utils.gfx.getText(defaults.description, defaults.size/2 + 'px ' + defaults.font, defaults.firstColor);
    descriptionText.x = x + defaults.size*1.5;
    descriptionText.y = y + defaults.size/1.5;
    descriptionText.textBaseline = 'alphabetic';


    container.addChild(descriptionText);

    //checkbox
    let checkbox = MTLG.utils.gfx.getShape();
    checkbox.x = x;
    checkbox.y = y;
    checkbox.graphics.setStrokeStyle(defaults.size/5);
    if(defaults.defaultVal){
      checkbox.graphics.beginStroke(defaults.secondColor).beginFill(defaults.firstColor).drawRect(0, 0, defaults.size, defaults.size);
    }else{
      checkbox.graphics.beginStroke(defaults.secondColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.size, defaults.size);
    }
    container.addChild(checkbox);


    container.on('click', function(evt){
      defaults.defaultVal = !defaults.defaultVal;
      clickOnFunction(defaults.defaultVal);
      if(defaults.defaultVal){
        container.removeChild(checkbox);
        container.addChild(checkbox);
        checkbox.graphics.beginStroke(defaults.secondColor).beginFill(defaults.firstColor).drawRect(0, 0, defaults.size, defaults.size);
      }else{
        container.removeChild(checkbox);
        container.addChild(checkbox);
        checkbox.graphics.beginFill(defaults.secondColor).drawRect(0, 0, defaults.size, defaults.size);
      }
    });

    return container;
  };

  /**
   * Creates a simple scrollbox in a new container and returns the container.
   * @param {object} properties A properties object that can specify all properties described below
   * @property {string} font The font that is used for the text above the scrollbox
   * @property {number} sizeX The x-size of the scrollbox
   * @property {number} sizeY The y-size of the scrollbox
   * @property {string} firstColor The background color of the scrollbox
   * @property {string} secondColor The stroke and text color of the scrollbox
   * @property {array} options An array of strings that represents the possible options
   * @property {number} defaultVal The index of the default option
   * @property {string} description The description text that is printed above the scrollbox
   * @param {function} clickOnFunction The function that is called when the value changes
   * @return {object} The container with the new scrollbox
   * @example
   * //Create a scrollbox of size 300x40 that logs its state to the console when it is changed
   * let scrollProp = {font: "Arial", sizeX: 300, sizeY: 40, firstColor: "red", secondColor: "grey", options: ["Test0", "Test1", "Test2", "Test3"], defaultVal: 1, description: "Scrollbox"};
   * var newScroll = MTLG.utils.uiElements.addScrollbox(scrollProp, function(value) {
   *   console.log("Scrollbox new Value: " + value);
   * });
   * //Set parameters of the new container
   * newScroll.x = 50;
   * newScroll.y = 700;
   * //Add the new container to a stage or container
   * stage.addChild(newScroll);
   * @memberof MTLG.utils.uiElements#
   */
  let addScrollbox = function(properties, clickOnFunction){
    let defaults = {
      font: "Arial",
      sizeX: 300,
      sizeY: 50,
      firstColor: "red",
      secondColor: "grey",
      options: [],
      defaultVal: 0,
      description: "",
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();

    if(!Array.isArray(defaults.options)){
      console.log("Scrollbox Error: List of choices not an array!");
      return;
    }
    if(defaults.defaultVal < 0 || defaults.defaultVal > defaults.options.length){
      defaults.defaultVal = 0;
      console.log("Scrollbox Error: Selected Element not available");
    }

    //description
    let descriptionText = MTLG.utils.gfx.getText(defaults.description, defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    descriptionText.x = x;
    descriptionText.y = y;
    descriptionText.textBaseline = 'alphabetic';
    container.addChild(descriptionText);

    //firstBox
    let firstBox = MTLG.utils.gfx.getShape();
    firstBox.x = x;
    firstBox.y = y + defaults.sizeY*0.5;
    firstBox.graphics.setStrokeStyle(1);
    firstBox.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeX, defaults.sizeY);
    container.addChild(firstBox);

    //secondBox
    let secondBox = MTLG.utils.gfx.getShape();
    secondBox.x = x;
    secondBox.y = y + defaults.sizeY*0.5 + defaults.sizeY;
    secondBox.graphics.setStrokeStyle(1);
    secondBox.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeX + defaults.sizeY, defaults.sizeY); //TODO length + width
    secondBox.shadow = new createjs.Shadow(defaults.firstColor, 0, 0, 7);

    //thirdBox
    let thirdBox = MTLG.utils.gfx.getShape();
    thirdBox.x = x;
    thirdBox.y = y + defaults.sizeY*0.5 + defaults.sizeY*2;
    thirdBox.graphics.setStrokeStyle(1);
    thirdBox.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeX, defaults.sizeY);
    container.addChild(thirdBox);

    //firstText
    let firstText = MTLG.utils.gfx.getText("", defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    firstText.x = x + 5;
    firstText.y = y + defaults.sizeY*1.2;
    firstText.textBaseline = 'alphabetic';
    firstText.maxWidth = defaults.sizeX * 4/5; //TODO
    if(defaults.defaultVal>0){
      firstText.text = defaults.options[defaults.defaultVal-1];
    }
    container.addChild(firstText);

    //secondText
    let secondText = MTLG.utils.gfx.getText("", defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    secondText.x = x + 5;
    secondText.y = y + defaults.sizeY*2.2;
    secondText.textBaseline = 'alphabetic';
    secondText.maxWidth = defaults.sizeX; //TODO
    if(defaults.options.length > defaults.defaultVal){
      secondText.text = defaults.options[defaults.defaultVal];
    }

    //thirdText
    let thirdText = MTLG.utils.gfx.getText("", defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    thirdText.x = x + 5;
    thirdText.y = y + defaults.sizeY*3.2;
    thirdText.textBaseline = 'alphabetic';
    thirdText.maxWidth = defaults.sizeX * 4/5; //TODO
    if(defaults.options.length > defaults.defaultVal + 1){
      thirdText.text = defaults.options[defaults.defaultVal+1];
    }
    container.addChild(thirdText);


    //upButton
    let upButton = MTLG.utils.gfx.getShape();
    upButton.x = x + defaults.sizeX;
    upButton.y = y + defaults.sizeY*0.5;
    upButton.graphics.setStrokeStyle(1);
    upButton.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeY, defaults.sizeY);

    upButton.on('click', function(evt){
      if(defaults.defaultVal > 0){
        defaults.defaultVal--;
        clickOnFunction(defaults.defaultVal);
        firstText.text = "";
        secondText.text = "";
        thirdText.text = "";
        container.removeChild(firstText);
        container.removeChild(secondText);
        container.removeChild(thirdText);
        if(defaults.defaultVal>0){
          firstText.text = defaults.options[defaults.defaultVal-1];
        }
        container.addChild(firstText);
        if(defaults.options.length > defaults.defaultVal){
          secondText.text = defaults.options[defaults.defaultVal];
        }
        container.addChild(secondText);
        if(defaults.options.length > defaults.defaultVal + 1){
          thirdText.text = defaults.options[defaults.defaultVal+1];
        }
        container.addChild(thirdText);
      }
    });
    container.addChild(upButton);

    //downButton
    let downButton = MTLG.utils.gfx.getShape();
    downButton.x = x + defaults.sizeX;
    downButton.y = y + defaults.sizeY*2.5;
    downButton.graphics.setStrokeStyle(1);
    downButton.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeY, defaults.sizeY);

    downButton.on('click', function(evt){
      if(defaults.defaultVal+1 < defaults.options.length){
        defaults.defaultVal++;
        clickOnFunction(defaults.defaultVal);
        firstText.text = "";
        secondText.text = "";
        thirdText.text = "";
        container.removeChild(firstText);
        container.removeChild(secondText);
        container.removeChild(thirdText);
        if(defaults.defaultVal>0){
          firstText.text = defaults.options[defaults.defaultVal-1];
        }
        container.addChild(firstText);
        if(defaults.options.length > defaults.defaultVal){
          secondText.text = defaults.options[defaults.defaultVal];
        }
        container.addChild(secondText);
        if(defaults.options.length > defaults.defaultVal + 1){
          thirdText.text = defaults.options[defaults.defaultVal+1];
        }
        container.addChild(thirdText);
      }
    });
    container.addChild(downButton);

    //selectedBackground
    let selectedBackground = MTLG.utils.gfx.getShape();
    selectedBackground.x = x + defaults.sizeX;
    selectedBackground.y = y + defaults.sizeY*1.5;
    selectedBackground.graphics.setStrokeStyle(1);
    selectedBackground.graphics.beginStroke(defaults.firstColor).beginFill(defaults.secondColor).drawRect(0, 0, defaults.sizeY, defaults.sizeY);
    //container.addChild(selectedBackground);

    //upArrow
    let upArrow = MTLG.utils.gfx.getText("↑", "bold " +  defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    upArrow.x = x + defaults.sizeX + 0.35*defaults.sizeY;
    upArrow.y = y + defaults.sizeY*1.1;
    upArrow.textBaseline = 'alphabetic';
    container.addChild(upArrow);

    //downArrow
    let downArrow = MTLG.utils.gfx.getText("↓", "bold " +  defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    downArrow.x = x + defaults.sizeX + 0.35*defaults.sizeY;
    downArrow.y = y + defaults.sizeY*3.1;
    downArrow.textBaseline = 'alphabetic';
    container.addChild(downArrow);

    //selectedArrow
    let selectedArrow = MTLG.utils.gfx.getText("←", "bold " +  defaults.sizeY/2 + 'px ' + defaults.font, defaults.firstColor);
    selectedArrow.x = x + defaults.sizeX + 0.2*defaults.sizeY;
    selectedArrow.y = y + defaults.sizeY*2.1;
    selectedArrow.textBaseline = 'alphabetic';
    //container.addChild(selectedArrow);

    //Add highlighted box last
    container.addChild(secondBox);
    container.addChild(secondText);

    return container;
  };

  /**
   * Creates a simple slider in a new container and returns the container.
   * @param {object} properties A properties object that can specify all properties described below
   * @property {string} font The font that is used for the text above and next to the slider
   * @property {number} x The x position of the slider in the container
   * @property {number} y The y position of the slider in the container
   * @property {number} sizeX Length of the slider
   * @property {number} sizeY Width of the slider
   * @property {string} firstColor The foreground and text color of the slider
   * @property {string} secondColor The background color of the slider
   * @property {number} min The minimum value of the slider
   * @property {number} max The maximum value of the slider
   * @property {number} stepSize The size of the steps the slider can take
   * @property {number} defaultVal Initial value of the slider
   * @property {string} description The description text that is printed above the slider
   * @param {function} clickOnFunction The function that is called when the value changes
   * @return {object} The container with the new slider
   * @example
   * //Create a slider of size 400x50 that logs its state to the console when it is changed
   * let sliderProp = {font: 'Arial', sizeX: 400, sizeY: 50, firstColor: 'red', secondColor: 'grey', min: 0, max: 10, stepSize: 1, defaultVal: 3, description:"Slider"};
   * var newSlider = MTLG.utils.uiElements.addSlider(sliderProp,function(val){
   *   console.log("Slider new value: " + val);
   * });
   * //Change properties of the new container
   * newSlider.x = 50;
   * newSlider.y = 100;
   * //Add the container to a stage or container
   * stage.addChild(newSlider);
   * @memberof MTLG.utils.uiElements#
   */
  let addSlider = function (properties, clickOnFunction){
    let defaults = {
      font: "Arial",
      sizeX: 400,
      sizeY: 50,
      firstColor: "red",
      secondColor: "grey",
      min: 0,
      max:10,
      stepSize: 1,
      defaultVal: 5,
      description: "",
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();

    //description
    let descriptionText = MTLG.utils.gfx.getText(defaults.description, defaults.sizeY + 'px ' + defaults.font, defaults.firstColor);
    descriptionText.x = x;
    descriptionText.y = y;
    descriptionText.textBaseline = 'alphabetic';
    container.addChild(descriptionText);


    //Function called on pressmove on slider and line
    let moveFunction = function(evt){
      let xy = container.globalToLocal(evt.stageX, evt.stageY);
      if(xy.x > x && xy.x < x + defaults.sizeX){
        let renderStep = defaults.sizeX / ((defaults.max-defaults.min) / defaults.stepSize);
        defaults.defaultVal = defaults.min + defaults.stepSize * Math.floor(xy.x / renderStep);
        //defaults.defaultVal = Math.round((((xy.x - x)/defaults.sizeX) * range + defaults.min) * Math.pow(10, defaults.stepSize)) / Math.pow(10, defaults.stepSize);
      }else if(xy.x < x){
        defaults.defaultVal = defaults.min;
        xy.x = x;
      }else{
        defaults.defaultVal = defaults.max;
        xy.x = x + defaults.sizeX;
      }
      slider.graphics.clear();
      container.removeChild(slider);
      container.addChild(slider);
      slider.graphics.beginFill(defaults.firstColor).drawCircle(xy.x, y+defaults.sizeY*1.5, defaults.sizeY);
      valueText.text = defaults.defaultVal;
      container.removeChild(valueText);
      container.addChild(valueText);
      //Callback only if value changed
      if(defaults.defaultVal !== lastValue){
        lastValue = defaults.defaultVal;
        clickOnFunction(defaults.defaultVal);
      }
    };

    //Slider Line
    let line = MTLG.utils.gfx.getShape();
    line.graphics.setStrokeStyle(defaults.sizeY);
    line.graphics.beginStroke(defaults.secondColor);
    line.graphics.moveTo(x,y+defaults.sizeY*1.5);
    line.graphics.lineTo(x+defaults.sizeX,y+defaults.sizeY*1.5);
    line.on("pressmove", moveFunction);
    container.addChild(line);

    //Slider
    let range = defaults.max-defaults.min;
    let slider = MTLG.utils.gfx.getShape();
    slider.graphics.beginFill(defaults.firstColor).drawCircle(x+(defaults.sizeX*((defaults.defaultVal-defaults.min)/range)), y+defaults.sizeY*1.5, defaults.sizeY);
    slider.on("pressmove", moveFunction);
    container.addChild(slider);

    //Value
    let valueText = MTLG.utils.gfx.getText(defaults.defaultVal, defaults.sizeY + 'px ' + defaults.font, defaults.firstColor);
    valueText.x = x+defaults.sizeX+2*defaults.sizeY;
    valueText.y = y+defaults.sizeY*1.5 + defaults.sizeY/3;
    valueText.textBaseline = 'alphabetic';
    container.addChild(valueText);

    //Remember last value
    let lastValue = defaults.defaultVal;


    return container;
  };

  /**
   * Returns a container containing the specified text.
   * Text is bounded by sizeX value in x direction.
   * If wrap is false the text will be shrunk, else wrapped.
   * The text is left-aligned in the container and starts at 0,0.
   * @param {object} properties A properties object that can specify all properties described below
   * @property {string} text A string containing the text.
   * @property {string} font Specifications for the new text e.g. "20px Arial"
   * @property {string} color The font color in a format that createjs recognizes e.g. "#ff7700" or "blue"
   * @property {number} sizeX The maximum width of the text
   * @property {number} sizeY The maximum height of the text (TODO: Not supported yet)
   * @property {boolean} wrap Determines if the text wraps (true) or shrinks (false) to maxWidth. Wraps after spaces
   * @return {object} The container with the new text object
   * @example
   * //Define properties for text object. See properties in docs to see what you can do.
   * let textProp = {text: "Your text here!", font: "50px Arial", color: "Black", sizeX: 50, sizeY: 100, wrap: true};
   * //Create the text (returns text in a container)
   * var textTest = MTLG.utils.uiElements.addText(textProp);
   * //Set the position of the container and add it to a stage
   * textTest.x = textTest.y = 800;
   * stage.addChild(textTest);
   * @todo sizeY
   * @todo font includes size only in this uiElement
   * @memberof MTLG.utils.uiElements#
   */
  let addText = function(properties){
    let defaults = {
      text: "",
      font: "Arial",
      color: "Black",
      sizeX: 0,
      sizeY: 0,
      wrap: false,
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();
    let text = MTLG.utils.gfx.getText(defaults.text, defaults.font, defaults.color);
    text.textBaseline = 'alphabetic';
    if(defaults.wrap){
      text.lineWidth = defaults.sizeX;
    }else{
      text.maxWidth = defaults.sizeX;
    }
    container.addChild(text);
    //container.text = text.text;
    Object.defineProperty(container, 'text', {
      get: function() { return text.text },
      set: function(v) { text.text = v; }
    });
    return container;
  }

  /**
   * Returns a container containing the specified radion buttons
   * @param {object} properties A properties object that can specify all properties described below
   * @property {string} description The description standing above the buttons
   * @property {array} options An array containing the different options
   * @property {number} defaultVal Default value of the radio button as array index
   * @property {number} sizeX The width of the container
   * @property {number} lineHeight The line height of all elements. There are three lines: The description, the buttons, and the option texts
   * @property {string} font Specifications for the description and option text e.g. "Arial"
   * @property {string} textColor The font color in a format that createjs recognizes e.g. "#ff7700" or "blue"
   * @property {string} firstColor The stroke color for radio buttons
   * @property {string} secondColor The fill color for radion buttons
   * @param {function} callback The callback function that is called when the value changes
   * @return {object} The container with the new text object
   * @example
   * //Define properties. This will create three radion buttons with the options "First", "Second" and "Third". It has an x-size of 500 and line height of 50.
   * let radioProp = {description: "Test Radio", options: ["First", "Second", "Third"], sizeX : 500, lineHeight: 50, color: "red"};
   * //Create actual container
   * var testRadio = MTLG.utils.uiElements.addRadioButton(radioProp, function(v){console.log(v)});
   * //Put container at different position and add to stage
   * testRadio.x = 700;
   * testRadio.y = 500;
   * stage.addChild(testRadio);
   * @memberof MTLG.utils.uiElements#
   */
  let addRadioButton = function(properties, callback){
    let defaults = {
      description: "",
      options: [],
      sizeX: 500,
      lineHeight: 50,
      font: "Arial",
      textColor: "Black",
      firstColor: "Red",
      secondColor: "White",
      defaultVal: 0,
    }
    Object.assign(defaults, properties);
    let container = new createjs.Container();
    let descProp = {text: defaults.description, font: defaults.lineHeight * 0.75 /*margin*/ + "px "+ defaults.font, color: defaults.textColor, sizeX: defaults.sizeX, lineHeight: defaults.lineHeight};
    let descText = addText(descProp);
    container.addChild(descText);
    let currentState = defaults.defaultVal;
    let optionGraphics = [];

    let getOption = function(size, val){
      var ret = MTLG.utils.gfx.getShape();
      ret.graphics.beginFill(defaults.secondColor).drawCircle(0,0,size);
      ret.graphics.beginStroke(defaults.firstColor).drawCircle(0,0,size);
      ret.addEventListener("click", function(){
        optionGraphics[currentState].graphics.beginFill(defaults.secondColor).drawCircle(0,0,size);
        currentState = val;
        optionGraphics[currentState].graphics.beginFill(defaults.firstColor).drawCircle(0,0,size);
        callback(currentState);
      });
      return ret;
    }

    //This next part does not make sense if there are no selection options
    if(defaults.options.length <= 0){return container};

    let optionSizeX = defaults.sizeX / defaults.options.length;
    let optionMarginX = defaults.sizeX / defaults.options.length * 0.1;
    for(let option in defaults.options){
      let cRad = defaults.lineHeight * 0.75 * 0.5; //Margin: .75, r not d: 0.5
      let cMarginY = (defaults.lineHeight - 2* cRad) /2; //Center vertically
      optionGraphics.push(getOption(cRad, option));
      if(option == defaults.defaultVal){
        optionGraphics[option].graphics.beginFill(defaults.firstColor).drawCircle(0,0,cRad);
      }
      optionGraphics[option].x = optionSizeX * (optionGraphics.length - 1) + optionSizeX / 2; //TODO: used length since option could be non-numeric? Ugly
      optionGraphics[option].y = defaults.lineHeight;
      container.addChild(optionGraphics[option]);
      let textProp = {text: defaults.options[option], font: defaults.lineHeight * 3/4 + "px "+ defaults.font, sizeX: optionSizeX - 2*optionMarginX, lineHeight: defaults.lineHeight, color: defaults.textColor};
      let text = addText(textProp);
      let tWidth = text.getBounds().width;
      text.x = optionSizeX * (optionGraphics.length - 1) + optionMarginX + (optionSizeX - 2*optionMarginX -tWidth)/2; //TODO: See above
      text.y = 2*defaults.lineHeight;
      container.addChild(text);
      //TODO?: Make text to buttons (so that clicking triggers event) + make all of this in above function?
    }

    return container;
  }

  return {
    addButton: addButton,
    addCheckbox: addCheckbox,
    addScrollbox: addScrollbox,
    addSlider: addSlider,
    addText: addText,
    addRadioButton: addRadioButton
  }
})();
module.exports = uiElements;
