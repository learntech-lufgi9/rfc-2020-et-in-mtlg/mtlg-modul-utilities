/**
 * basic graph structure
 * @example
 * //First create a graph using the stage or container stageC (e.g. the stage container to ensure correct scaling)
 * var myGraph = new MTLG.utils.graph(stageC);
 * //Add five nodes as an array of values
 * myGraph.addNodes([1,2,3,4,5]);
 * //Create random edges between all existing nodes
 * myGraph.randomEdges();
 * //Add and distribute the nodes evenly on stageC
 * myGraph.addToStage();
 * myGraph.distributeOnStage();
 * //Use the update function to update the visual representation of the graph
 * //(This function is also called in distributeOnStage)
 * myGraph.updateGraph();
 * @class graph
 * @memberof MTLG.utils
 * @param {createjs.Stage} pStage
 */
function graph(pStage) {
  this.nodes = [];
  this.edges = [];
  this.node_id = 0;
  this.edge_id = 0;
  this.mStage = pStage?pStage:MTLG.getStage();
}

graph.prototype = {
  /**
   * Adds a node to the graph
   * @memberof MTLG.utils.graph#
   * @param {type} value Set the value of the node.
   **/
  addNode: function(value) {
    var new_node;

    function node(node_id,pStage) {

      function draggable(e) {
        if (!e.tangible) {
          let coords = pStage.globalToLocal(e.stageX, e.stageY);
          this.x = coords.x;
          this.y = coords.y;
          this.node.updateEdges();
        }
      }

      function graphics() {
        this.kind = "circle"
        this.color = "red";
        this.radius = 30;
        this.width = 60;
        this.height = 60;
        this.shape = MTLG.utils.gfx.getShape();
        this.shape.on("pressmove", draggable);
        this.stx = 0;
        this.sty = 0;
      }

      graphics.prototype = {

        setPosition: function(posX, posY) {
          this.shape.x = posX;
          this.shape.y = posY;
        },

        getPosX: function() {
          return this.shape.x;
        },

        getPosY: function() {
          return this.shape.y;
        },

        setColor: function(color) {
          this.color = color;
        },

        setSize: function(width, height, radius) {
          this.width = width;
          this.height = height;
          this.radius = radius;
        },

        setThreshhold: function(x, y) {
          if (x && y) {
            this.stx = x;
            this.sty = y;
          } else {
            if (this.kind === "rectangle") {
              this.stx = this.width / 2;
              this.sty = this.height / 2;
            }
            if (this.kind === "circle") {
              this.stx = 0;
              this.sty = 0;
            }
          }
        }
      }

      this.id = node_id;
      this.edges = [];
      this.value = value;
      this.graphics = new graphics();
      this.graphics.shape.node = this;
    }

    node.prototype = {
      addEdge: function(edge) {
        this.edges.push(edge);
      },

      rmEdge: function(edge) {
        this.edges = this.edges.filter(function(el) {
          return el.id !== edge.id;
        });
      },

      updateEdges: function() {
        this.edges.forEach(function(e) {
          e.updatePos();
        });
      },

      getGraphicsNode: function() {
        return this.graphics.shape.graphics;
      }
    }

    new_node = new node(this.node_id,this.mStage);
    this.nodes.push(new_node);
    this.node_id = this.node_id + 1;
    return new_node;
  },

  /**
   * Adds for each value in the values array a node to the graph
   * @memberof MTLG.utils.graph#
   * @param {array} values Array of values representing nodes.
   **/
  addNodes: function(values) {
    var gr = this;
    try {
      if (!values.length) {
        throw "Use an array of values of notes ";
      } else {
        values.forEach(function(el) {
          gr.addNode(el);
        });
      }
    } catch (err) {
      console.log("In function addNotes: " + err);
      console.log("No node was added.");
    } finally {
      console.log(this.nodes);
    }
  },

  /**
   * Deletes a node of the graph
   * @memberof MTLG.utils.graph#
   * @param {type} node Removes the node out of the graph.
   **/
  rmNode: function(node) {
    var gr = this;
    node.graphics.shape.graphics.clear();
    node.edges.forEach(function(el) {
      gr.rmEdges(el);
    });
    this.nodes = this.nodes.filter(function(el) {
      return el.id !== node.id;
    });
  },

  /**
   * Adds an edge to the graph
   * @memberof MTLG.utils.graph#
   * @param {type} node1 Start node.
   * @param {type} node2 End node.
   **/
  addEdge: function(node1, node2) {
    function edge(edge_id) {

      function graphics() {
        this.color = "black";
        this.style = 3;
        this.shape = MTLG.utils.gfx.getShape();
        this.startX = 0;
        this.startY = 0;
        this.endX = 0;
        this.endY = 0;
      }

      graphics.prototype = {
        setStart: function(x, y) {
          this.startX = x;
          this.startY = y;
        },

        setEnd: function(x, y) {
          this.endX = x;
          this.endY = y;
        }
      }

      try {
        if (!node1) {
          throw "node1 missing"
        }
        if (!node2) {
          throw "node2 missing"
        }
        this.id = edge_id;
        this.node1 = node1;
        this.node1.addEdge(this);
        this.node2 = node2;
        this.node2.addEdge(this);
        this.graphics = new graphics();
        this.graphics.shape.edge = this; // mapping
      } catch (err) {
        console.log("In function addEdge: " + err);
      }
    }

    edge.prototype = {
      updatePos: function() {
        var g = this.graphics,
          ng1 = this.node1.graphics,
          ng2 = this.node2.graphics;
        g.setEnd(ng2.getPosX() + ng2.stx, ng2.getPosY() + ng2.sty);
        g.setStart(ng1.getPosX() + ng1.stx, ng1.getPosY() + ng1.sty);
        g.shape.graphics.clear();
        g.shape.graphics.beginStroke(g.color).moveTo(g.endX, this.graphics.endY).lineTo(g.startX, g.startY).endStroke();
      }
    }

    this.edges.push(new edge(this.edge_id));
    this.edge_id = this.edge_id + 1;
    return this;
  },

  /**
   * Adds edges to the graph.
   * @memberof MTLG.utils.graph#
   * @param {type} edges An array of edges [[n0,n1][n1,n2],...].
   **/
  addEdges: function(edges) {
    var gr = this;
    try {
      if (!edges.length) {
        throw "Use an array of edges [[n0,n1][n1,n2]].";
      } else {
        edges.forEach(function(el) {
          if (el.length !== 2) {
            throw "An edge has only a start and a end-node [n0,n1].";
          } else {
            gr.addEdge(el[0], el[1]);
          }
        });
      }
    } catch (err) {
      console.log("In function addNotes: " + err);
      console.log("No node was added.");
    } finally {
      console.log(this.nodes);
    }
  },

  /**
   * Removes an edge of the graph.
   * @memberof MTLG.utils.graph#
   * @param {type} edge Edge to remove.
   **/
  rmEdges: function(edge) {
    edge.graphics.shape.graphics.clear();
    this.nodes.forEach(function(el) {
      el.rmEdge(edge);
    });
    this.edges = this.edges.filter(function(el) {
      return el.id !== edge.id;
    });
  },

  /**
   * Returns the first node, if it exists. Or false, if not.
   * @memberof MTLG.utils.graph#
   * @return {type} The first node or false.
   **/
  getFirstNode: function() {
    if (this.nodes[0]) {
      return this.nodes[0];
    }
    return false;
  },

  /**
   * Returns the last node, if it exists. Or false, if not.
   * @memberof MTLG.utils.graph#
   * @return {type} The last node or false.
   **/
  getLastNode: function() {
    if (this.nodes[0]) {
      return this.nodes[this.nodes.length - 1];
    }
    return false;
  },

  /**
   * Returns the first edge, if it exists. Or false, if not.
   * @memberof MTLG.utils.graph#
   * @return {type} The first edge or false.
   **/
  getFirstEdge: function() {
    if (this.edges[0]) {
      return this.edges[0];
    }
    return false;
  },

  /**
   * Returns the last edge, if it exists. Or false, if not.
   * @memberof MTLG.utils.graph#
   * @return {type} The last edge or false.
   **/
  getLastEdge: function() {
    if (this.edges[0]) {
      return this.edges[this.edges.length - 1];
    }
    return false;
  },

  /**
   * Updates the position of the edges.
   * @memberof MTLG.utils.graph#
   **/
  updateEdges: function() {
    this.edges.forEach(function(el, index, array) {
      el.updatePos();
    });
  },

  /**
   * Updates a general property or a graphics property of all nodes with a specific value.
   * @param {type} p Property to update.
   * @param {type} value Value to set to property.
   * @param {type} graphics false for general property | true for graphics property.
   * @memberof MTLG.utils.graph#
   **/
  setAllNodesProperties: function(p, value, graphics) {
    if (p && value) {
      this.nodes.forEach(function(el, index, array) {
        var o = el;
        if (graphics) {
          o = el.graphics;
        }
        Object.keys(o).forEach(function(key, index) {
          if (key === p) {
            o[key] = value;
          }
        });
      });
    }
  },

  /**
   * Updates a general property or a graphics property of all edges with a specific value.
   * @param {type} p Property to update.
   * @param {type} value Value to set to property.
   * @param {type} graphics false for general property | true for graphics property.
   * @memberof MTLG.utils.graph#
   **/
  setAllEdgesProperties: function(p, value, graphics) {
    if (p && value) {
      this.edges.forEach(function(el, index, array) {
        var o = el;
        if (graphics) {
          o = el.graphics;
        }
        Object.keys(o).forEach(function(key, index) {
          if (key === p) {
            o[key] = value;
          }
        });
      });
    }
  },

  /**
   * Set the graphics of each nodes.<br>
   * TODO set graphics with a function and circle as fallback.
   * @memberof MTLG.utils.graph#
   **/
  setGraphicsNodes: function() {
    this.nodes.forEach(function(el, index, array) {
      var g = el.graphics;
      g.shape.graphics.clear();
      if (g.kind === "circle") {
        g.shape.graphics.beginFill(g.color).drawCircle(0, 0, g.radius).endFill();
      }
      if (g.kind === "rectangle") {
        g.shape.graphics.beginFill(g.color).drawRect(0, 0, g.width, g.height).endFill();
      }
      g.setThreshhold();
    });
  },

  /**
   * Set the graphics of each edge.<br>
   * TODO set graphics with a function and standard strike as fallback.
   * @memberof MTLG.utils.graph#
   **/
  setGraphicsEdges: function() {
    this.edges.forEach(function(el, index, array) {
      el.graphics.shape.graphics.setStrokeStyle(el.graphics.style);
    });
  },

  /**
   * Adds the graph to the stage.
   * @memberof MTLG.utils.graph#
   **/
  addToStage: function() {
    var graphics = [this.edges, this.nodes];
    var gr = this;
    var c;
    graphics.forEach(function(g, index, array) {
      g.forEach(function(el, index, array) {
        if (!el.graphics.onstage) {
          if (!c) { // is an edge
            if (gr.getLastEdge().graphics.shape) {
              this.mStage.addChild(el.graphics.shape);
              this.mStage.setChildIndex(el.graphics.shape, 0);
            }
          } else { // is a node
            this.mStage.addChild(el.graphics.shape);
            this.mStage.setChildIndex(el.graphics.shape, this.mStage.getNumChildren() - 1);
          }
        }
        el.graphics.onstage = true;
      }.bind(this));
      c = true;
    }.bind(this));
  },

  /**
   * Removes the graph.
   * @memberof MTLG.utils.graph#
   **/
  rmGraph: function() {
    var gr = this;
    this.nodes.forEach(function(el) {
      gr.rmNode(el);
    });
    console.log(this.edges + " should be 0."); // TODO Debug
    this.edges.forEach(function() {

    });
    this.node_id = 0;
    this.edge_id = 0;
  },

  /**
   * Updates the graph graphics.<br>
   * TODO do not add all edges if only one edge is new
   * @memberof MTLG.utils.graph#
   **/
  updateGraph: function() {
    this.addToStage();
    this.setGraphicsNodes();
    this.setGraphicsEdges();
    this.updateEdges();
  },

  /**
   * Adds edges to nodes randomly (80 % )
   * @memberof MTLG.utils.graph#
   **/
  randomEdges: function() {
    var gr = this;
    var random_node;
    var maxEdges = gr.nodes.length - 1;

    if (maxEdges > 6) {
      maxEdges = 6;
    }

    this.nodes.forEach(function(el) {
      if (Math.random() < 0.8 && gr.nodes.length > 1) {
        random_node = gr.nodes[Math.floor(Math.random() * maxEdges)];
        while (random_node.id === el.id) {
          random_node = gr.nodes[Math.floor(Math.random() * maxEdges)];
        }
        gr.addEdge(el, random_node);
      }
    });
  },

  /**
   * Distribute the nodes on the screen (alpha version).<br>
   * TODO implement a beta version
   * @memberof MTLG.utils.graph#
   **/
  distributeOnStage: function() {
    var x = 50,
      y = 50;
    this.nodes.forEach(function(el, index, array) {
      el.graphics.setPosition(x, y);
      x = x + 100;
      y = y + 100;
    });
    this.updateGraph();
  }
}
module.exports = graph;
