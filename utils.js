/**
 * @Date:   2019-04-09T11:20:16+02:00
 * @Last modified time: 2020-03-05T10:53:39+01:00
 */


/**
 * Utilities modul of the mtlg-framework.
 * Collection of hopefully usefull data structures and utilities
 *
 * @namespace utils
 * @memberof MTLG
 *
 **/
"use strict";
let utils = {};
//Load basic graph utilities
utils.graph = require('./graph.js');
//Load complex graph utilities
utils.complexGraph = require('./complexGraph.js');
//Load UI Elements
utils.uiElements = require('./uiElements.js');
//Load Feedback module
utils.fbm = require('./fbm.js');
//Load Timer module
utils.timer = require('./timer.js');
//Load inactivity module
utils.inactivity = require('./inactivity.js');
//Load collision detection module
utils.collision = require('./collision.js');
//Load graphics effects
utils.gfx = require('./gfx.js');
//load qrcode module
utils.qrcode = require('./qrcode.js');
//load inputkeyboard module
utils.inputkeyboards = require('./inputKeyboards.js');
//load movable module
utils.movable = require('./movables.js');
//load apriltags module
utils.apriltag = require('./apriltag.js');

//Add utilities to MTLG object to make them available
MTLG.utils = utils;

function init(objects,callback) {
  // Init inactivity timer
  utils.inactivity.init(objects,callback);
  // the feedback module needs to be initialized
  utils.fbm.init(objects,callback);
  utils.inputkeyboards.init(objects);
  return "fbm-loaded"; //Gibt Identifier des Callbacks für fbm zurück TODO: Nötig?
}
MTLG.addModule(init);
