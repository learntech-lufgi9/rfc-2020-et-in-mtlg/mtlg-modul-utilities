var aprilTagModule = (function() {
  var families = {
    "tag16h5": {
      count: 30,
      name: "tag16_05_"
    },
    "tag25h5": {
      count: 35,
      name: "tag25_09_"
    },
    "tag36h11": {
      count: 587,
      name: "tag36_11_"
    },
    "tagCircle21h7": {
      count: 38,
      name: "tagCircle21_7_"
    }
  }

  /**
   * createApriltag - This function creates a apriltag
   *
   * @param  {String} family         The family of the tag
   * @param  {function} id           Id of the tag within the family
   * @param  {Object} [options = {}] Object with options like width and height
   */
  var getApriltag = function(family, id, options = {}) {
    // add leading zeros
    var width = 5;
    var stringNumber = "";
    width -= id.toString().length;
    if ( width > 0 )
    {
      stringNumber += new Array( width + (/\./.test( id ) ? 2 : 1) ).join( '0' ) + id;
    } else {
      stringNumber += id;
    }

    var tag = MTLG.assets.getBitmap("img/apriltags/" + family + "/" + families[family].name + stringNumber + ".png");
    if(options.width) {
      tag.scaleX = options.width / tag.image.width;
    }
    if(options.height) {
      tag.scaleY = options.height / tag.image.height;
    }

    return tag;
  };

  return {
    getFamilies: () => {return families;},
    getApriltag: getApriltag,
  }
})();



module.exports = aprilTagModule;
