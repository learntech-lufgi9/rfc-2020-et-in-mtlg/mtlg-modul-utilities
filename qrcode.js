/**
 * @Date:   2019-04-17T14:01:50+02:00
 * @Last modified time: 2019-05-29T08:49:35+02:00
 */

var QRCode = require('qrcode');

 /**
  * createQRCode - This function creates a QRCode and a Bitmap with the QRCode
  * as content. The function calls the callback with the bitmap as parameter.
  *
  * @param  {String} text           content of the QRCode, for example "https://mtlg-framework.gitlab.io/"
  * @param  {function} cb           Callback which will be called with an object as only parameter: {success: boolean, bitmap: Bitmap, reason?: error}
  * @param  {Object} [options = {}] Object with options like version, errorCorrectionLevel, maskPattern, toSJISFunc, margin, scale, width. See more https://github.com/soldair/node-qrcode#qr-code-options
  */
var createQRCode = function(text, cb, options = {}) {
  QRCode.toDataURL(text, options, function (err, url) {
    if(!err) {
      // transform url to bitmap
      var asset = new createjs.Bitmap(url);

      // set bounds
      var width = 148; // default width
      if(options.width) width = options.width;
      asset.setBounds(0, 0, width, width);

      if(options.showText) {
        var container = new createjs.Container();
        container.addChild(asset);

        var textObj = MTLG.utils.uiElements.addText({text: text, sizeX: width, wrap: true, font: "50px Arial"});
        var canvas = document.getElementById("canvasObject");
        var c = canvas.getContext("2d");
        console.log(c.measureText(text));
        console.log(textObj);
        container.addChild(textObj);

        asset = container;
      }
      cb({success: true, bitmap: asset});
    } else {
      cb({success: false, bitmap: null, reason: err});
    }

  });
};

module.exports = {
  createQRCode: createQRCode
}
