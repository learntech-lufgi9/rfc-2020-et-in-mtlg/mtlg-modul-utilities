/**
 * @Date:   2020-03-05T08:35:56+01:00
 * @Last modified time: 2020-03-26T15:16:54+01:00
 */



var inputKeyboards = (function() {
  var globalOptions;

  var init = function(opt) {
    globalOptions = opt; // saves game options
    console.log("loaded");
    return "input keyboards loaded"; //Gibt Identifier des Callbacks zurück
  };

  var Creator = (function() {

    function createInputField(options, method) {
      // create container for the whole input object
      var container = new createjs.Container();
      container.x = options.x;
      container.y = options.y;
      container.width = options.w;
      container.height = options.h;

      var margin = 2;
      if (options.enterButton) {
        var inputWidth = options.w * 4 / 5;
        var enterWidth = options.w * 1 / 5 - margin;
      } else {
        var inputWidth = options.w;
      }
      // create input field
      let inputCon = new createjs.Container();
      let shape = MTLG.utils.gfx.getShape();
      shape.graphics.beginStroke("#000000").beginFill(options.inputfieldcolor).drawRect(0, 0, inputWidth, options.h);
      inputCon.addChild(shape);
      let placeholderText = MTLG.utils.uiElements.addText({
        text: options.placeholder,
        font: options.h / 2 + 'px Arial'
      });
      placeholderText.y = options.h * 3 / 4;
      inputCon.addChild(placeholderText);
      inputCon.x = 0;
      inputCon.y = 0;

      inputCon._text = placeholderText;
      inputCon._placeholder = options.placeholder;
      container._inputfield = inputCon;
      container.addChild(inputCon);

      container._inputfield.on("click", function() {

        method.onClickFunction(container);
      });

      if (options.enterButton) {
        let btnProp = {
          text: "Ok",
          sizeX: enterWidth,
          sizeY: options.h
        }
        let enterButton = MTLG.utils.uiElements.addButton(btnProp, function() {
          options.callback(method.getText());
        });
        enterButton.regX = 0;
        enterButton.regY = 0;
        enterButton.x = inputWidth + margin;

        container.addChild(enterButton);
      }

      method.setCallback(options.callback);
      return container;
    }

    return {
      createInputField: createInputField,
    }
  })();

  var Controller = (function() {
    var config;
    var method;

    function getInputField(options, method = null) {
      if (method == null) {
        defaultMethod();
      } else {
        setMethod(method);
      }
      var inputFieldOptions = {
        x: 0,
        y: 0,
        w: globalOptions.width * 1 / 8,
        h: globalOptions.height * 1 / 20,
        placeholder: "Click/Touch for enter input",
        callback: console.log,
        inputfieldcolor: "white",
        enterButton: true,
      }
      inputFieldOptions = Object.assign(inputFieldOptions, method.getInputFieldOptions());
      inputFieldOptions = Object.assign(inputFieldOptions, options);
      method.setInputFieldOptions(inputFieldOptions);

      var inputField = Creator.createInputField(inputFieldOptions, method);

      return inputField;
    }

    function setMethod(method) {
      method = method;
    }

    function defaultMethod() {
      method = new DistributedDisplayKeyboard();
    }


    return {
      getInputField: getInputField,
      setMethod: setMethod,
    };
  })();

  var Methods = (function() {

    var HardwareKeyboard = function(notPrintingMultiKeys, notLoggedKeys, charMappingCtrlAlt) {
      var wasActivated = false;
      var active = false;
      var keysPressed = [];
      var inputfield;
      var inputFieldOptions = {
        enterButton: true,
        enterPress: true
      }
      var callback;

      var hardwareKeyboardOptions = {

      }

      var activate = () => {
        inputfield.shadow = new createjs.Shadow('#4ac6e3', 0, 0, 50);
        active = true;
        document.addEventListener('keyup', onKeyUpFunction);
        document.addEventListener('keydown', onKeyDownFunction);
        setTimeout(() => {document.addEventListener('click', deactivate)}, 0);
      }

      var deactivate = () => {
        inputfield.shadow = null;
        active = false;
        document.removeEventListener('keyup', onKeyUpFunction);
        document.removeEventListener('keydown', onKeyDownFunction);
        document.removeEventListener('click', deactivate);
      }

      var setCallback = (newCallback) => {
        callback = newCallback;
      }

      var getText = () => {
        return inputfield._text.text;
      }

      var getInputFieldOptions = () => {
        return inputFieldOptions;
      }

      var setInputFieldOptions = (options) => {
        inputFieldOptions = options;
      }

      var onClickFunction = (container) => {
        console.log('Hardwarekeyboard method on click')
        inputfield = container._inputfield;
        if (!wasActivated) {
          wasActivated = true;
          inputfield._text.text = "";
        }
        toggleActivation();
      }

      let notPrintingMultiKeysDefault = [
        "Meta", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11",
        "F12", "NumLock", "ArrowUp", "ArrowLeft", "ArrowDown", "ArrowRight",
        "Home", "End", "PageUp", "PageDown", "Clear", "Insert", "Delete", "Enter",
        "PrintScreen", "ScrollLock", "Pause", "Dead", "Alt", "Escape",
        "Shift", "CapsLock", "Control", "AltGraph", "Backspace", "NumLock", "ContextMenu"
      ];

      var onKeyUpFunction = (e) => {
        // all invalid keys that might be pressed and now released
        let invalidKeys = notPrintingMultiKeys || notPrintingMultiKeysDefault;

        // all character mappings due to AltGraph or Control+Alt
        let charMapping = charMappingCtrlAlt || new Map([
          ["<", "|"],
          ["+", "~"],
          ["7", "{"],
          ["8", "["],
          ["9", "]"],
          ["0", "}"],
          ["ß", "\\"],
          ["e", "€"],
          ["q", "@"],
          ["m", "µ"]
        ]);

        let key = e.key;

        if (active) {
          // checks for Ctrl+Alt or Ctrl+AltGr pressed
          if (charMapping.has(key) &&
            (keysPressed.includes("Control") &&
              (keysPressed.includes("AltGraph") ||
                keysPressed.includes("Alt"))
            )
          ) {
            key = charMapping.get(key);
          }

          if (key == 'Enter') {
            if(this.inputFieldOptions.enterPress) callback(getText());
            return;
          }
          if (!invalidKeys.includes(key)) {
            inputfield._text.text += key;
          } else if (key == "Backspace") {
            inputfield._text.text = inputfield._text.text.slice(0, -1);
          }
        }

        if (keysPressed.includes(key)) {
          keysPressed = keysPressed.filter((v, i, arr) => {
            return v != key;
          });
        }
      }

      var onKeyDownFunction = (e) => {
        // all forbidden chars/multi-key-presses
        let forbiddenChars = [
          " ", "\\", "{", "[", "]", "}", "~", "|", "³", "²", "€", "@", "µ"
        ];
        // all invalid keys which will not be logged
        let invalidKeys = notLoggedKeys || [
          "Meta", "NumLock", "ArrowUp", "Clear", "ArrowLeft",
          "ArrowDown", "ArrowRight", "Home", "End", "PageUp", "PageDown", "Insert",
          "Delete", "Enter", "PrintScreen", "ScrollLock", "Pause", "F1", "F2", "F3",
          "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "ContextMenu"
        ];

        if (!keysPressed.includes(e.key) && !forbiddenChars.includes(e.key) && !invalidKeys.includes(e.key)) {
          keysPressed.push(e.key);
        }
        // Maybe use preventDefault in production mode
        // e.preventDefault();
        return;
      }

      var toggleActivation = () => {
        if (!active) {
          activate();
        } else {
          deactivate();
        }
      }

      return {
        getInputFieldOptions: getInputFieldOptions,
        setInputFieldOptions: setInputFieldOptions,
        onClickFunction: onClickFunction,
        getText: getText,
        setCallback: setCallback
      }
    }

    var VirtualKeyboardController = (function() {
      var layouts = {
        "numbers": function(options, callback) {
          var keys = [["1", "2", "3"], ["4", "5", "6"], ["7", "8", "9"], ["Close","0", "Enter"]];
          return new Keyboard(options, callback, keys);
        },
        "alphanumeric": function(options, callback) {
          var keys = [
            [
              {key: '1', capitalKey: '1', longPress: [], longPressCapital: [], keySize: 1},
              {key: '2', capitalKey: '2', longPress: [], longPressCapital: [], keySize: 1},
              {key: '3', capitalKey: '3', longPress: [], longPressCapital: [], keySize: 1},
              {key: '4', capitalKey: '4', longPress: [], longPressCapital: [], keySize: 1},
              {key: '5', capitalKey: '5', longPress: [], longPressCapital: [], keySize: 1},
              {key: '6', capitalKey: '6', longPress: [], longPressCapital: [], keySize: 1},
              {key: '7', capitalKey: '7', longPress: [], longPressCapital: [], keySize: 1},
              {key: '8', capitalKey: '8', longPress: [], longPressCapital: [], keySize: 1},
              {key: '9', capitalKey: '9', longPress: [], longPressCapital: [], keySize: 1},
              {key: '0', capitalKey: '0', longPress: [], longPressCapital: [], keySize: 1},

            ],
            [
              {key: 'q', capitalKey: 'Q', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'w', capitalKey: 'W', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'e', capitalKey: 'E', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'r', capitalKey: 'R', longPress: [], longPressCapital: [], keySize: 1},
              {key: 't', capitalKey: 'T', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'z', capitalKey: 'Z', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'u', capitalKey: 'U', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'i', capitalKey: 'I', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'o', capitalKey: 'O', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'p', capitalKey: 'P', longPress: [], longPressCapital: [], keySize: 1},
            ],
            [
              {key: 'Empty', keySize: 0.5},
              {key: 'a', capitalKey: 'A', longPress: [], longPressCapital: [], keySize: 1},
              {key: 's', capitalKey: 'S', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'd', capitalKey: 'D', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'f', capitalKey: 'F', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'g', capitalKey: 'G', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'h', capitalKey: 'H', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'j', capitalKey: 'J', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'k', capitalKey: 'K', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'l', capitalKey: 'L', longPress: [], longPressCapital: [], keySize: 1},
            ],
            [
              {key: 'Shift', capitalKey: 'Shift', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'y', capitalKey: 'Y', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'x', capitalKey: 'X', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'c', capitalKey: 'C', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'v', capitalKey: 'V', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'b', capitalKey: 'B', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'n', capitalKey: 'N', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'm', capitalKey: 'M', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'Backspace', capitalKey: 'Backspace', longPress: [], longPressCapital: [], keySize: 2},
            ],
            [
              {key: 'Close', capitalKey: 'Close', longPress: [], longPressCapital: [], keySize: 1},
              {key: ',', capitalKey: ',', longPress: [], longPressCapital: [], keySize: 1},
              {key: ' ', capitalKey: ' ', longPress: [], longPressCapital: [], keySize: 5},
              {key: '.', capitalKey: '.', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'Enter', capitalKey: 'Enter', longPress: [], longPressCapital: [], keySize: 2},
            ]
          ];
          return new AdvancedKeyboard(options, callback, keys);
        },
        "alphabet": function(options, callback) {
          var keys = [
            [
              {key: 'q', capitalKey: 'Q', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'w', capitalKey: 'W', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'e', capitalKey: 'E', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'r', capitalKey: 'R', longPress: [], longPressCapital: [], keySize: 1},
              {key: 't', capitalKey: 'T', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'z', capitalKey: 'Z', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'u', capitalKey: 'U', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'i', capitalKey: 'I', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'o', capitalKey: 'O', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'p', capitalKey: 'P', longPress: [], longPressCapital: [], keySize: 1},
            ],
            [
              {key: 'Empty', keySize: 0.5},
              {key: 'a', capitalKey: 'A', longPress: [], longPressCapital: [], keySize: 1},
              {key: 's', capitalKey: 'S', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'd', capitalKey: 'D', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'f', capitalKey: 'F', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'g', capitalKey: 'G', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'h', capitalKey: 'H', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'j', capitalKey: 'J', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'k', capitalKey: 'K', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'l', capitalKey: 'L', longPress: [], longPressCapital: [], keySize: 1},
            ],
            [
              {key: 'Shift', capitalKey: 'Shift', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'y', capitalKey: 'Y', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'x', capitalKey: 'X', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'c', capitalKey: 'C', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'v', capitalKey: 'V', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'b', capitalKey: 'B', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'n', capitalKey: 'N', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'm', capitalKey: 'M', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'Backspace', capitalKey: 'Backspace', longPress: [], longPressCapital: [], keySize: 2},
            ],
            [
              {key: 'Close', capitalKey: 'Close', longPress: [], longPressCapital: [], keySize: 2},
              {key: ' ', capitalKey: ' ', longPress: [], longPressCapital: [], keySize: 5.5},
              {key: 'Enter', capitalKey: 'Enter', longPress: [], longPressCapital: [], keySize: 2.5},
            ]
          ];
          return new AdvancedKeyboard(options, callback, keys);
        },
        "original": function(options, callback) {
          var keys = [
            [
              {key: '1', capitalKey: '1', longPress: [], longPressCapital: [], keySize: 1},
              {key: '2', capitalKey: '2', longPress: [], longPressCapital: [], keySize: 1},
              {key: '3', capitalKey: '3', longPress: [], longPressCapital: [], keySize: 1},
              {key: '4', capitalKey: '4', longPress: [], longPressCapital: [], keySize: 1},
              {key: '5', capitalKey: '5', longPress: [], longPressCapital: [], keySize: 1},
              {key: '6', capitalKey: '6', longPress: [], longPressCapital: [], keySize: 1},
              {key: '7', capitalKey: '7', longPress: [], longPressCapital: [], keySize: 1},
              {key: '8', capitalKey: '8', longPress: [], longPressCapital: [], keySize: 1},
              {key: '9', capitalKey: '9', longPress: [], longPressCapital: [], keySize: 1},
              {key: '0', capitalKey: '0', longPress: [], longPressCapital: [], keySize: 1},

            ],
            [
              {key: 'q', capitalKey: 'Q', longPress: ['%'], longPressCapital: ['%'], keySize: 1},
              {key: 'w', capitalKey: 'W', longPress: ['^'], longPressCapital: ['^'], keySize: 1},
              {key: 'e', capitalKey: 'E', longPress: ['~', 'é', 'è'], longPressCapital: ['~', 'É', 'È'], keySize: 1},
              {key: 'r', capitalKey: 'R', longPress: ['|'], longPressCapital: ['|'], keySize: 1},
              {key: 't', capitalKey: 'T', longPress: ['['], longPressCapital: ['['], keySize: 1},
              {key: 'z', capitalKey: 'Z', longPress: [']'], longPressCapital: [']'], keySize: 1},
              {key: 'u', capitalKey: 'U', longPress: ['ü', '<'], longPressCapital: ['Ü', '<'], keySize: 1},
              {key: 'i', capitalKey: 'I', longPress: ['>'], longPressCapital: ['>'], keySize: 1},
              {key: 'o', capitalKey: 'O', longPress: ['ö', '{'], longPressCapital: ['Ö', '{'], keySize: 1},
              {key: 'p', capitalKey: 'P', longPress: ['}'], longPressCapital: ['}'], keySize: 1},
            ],
            [
              {key: 'Empty', keySize: 0.5},
              {key: 'a', capitalKey: 'A', longPress: ['@', 'ä'], longPressCapital: ['@', 'Ä'], keySize: 1},
              {key: 's', capitalKey: 'S', longPress: ['ß', '#'], longPressCapital: ['ß', '#'], keySize: 1},
              {key: 'd', capitalKey: 'D', longPress: ['&'], longPressCapital: ['&'], keySize: 1},
              {key: 'f', capitalKey: 'F', longPress: ['*'], longPressCapital: ['*'], keySize: 1},
              {key: 'g', capitalKey: 'G', longPress: ['-'], longPressCapital: ['-'], keySize: 1},
              {key: 'h', capitalKey: 'H', longPress: ['+'], longPressCapital: ['+'], keySize: 1},
              {key: 'j', capitalKey: 'J', longPress: ['='], longPressCapital: ['='], keySize: 1},
              {key: 'k', capitalKey: 'K', longPress: ['('], longPressCapital: ['('], keySize: 1},
              {key: 'l', capitalKey: 'L', longPress: [')'], longPressCapital: [')'], keySize: 1},
            ],
            [
              {key: 'Shift', capitalKey: 'Shift', longPress: [], longPressCapital: [], keySize: 1},
              {key: 'y', capitalKey: 'Y', longPress: ['_'], longPressCapital: ['_'], keySize: 1},
              {key: 'x', capitalKey: 'X', longPress: ['$', '€'], longPressCapital: ['$', '€'], keySize: 1},
              {key: 'c', capitalKey: 'C', longPress: ['"'], longPressCapital: ['"'], keySize: 1},
              {key: 'v', capitalKey: 'V', longPress: ['\''], longPressCapital: ['\''], keySize: 1},
              {key: 'b', capitalKey: 'B', longPress: [':'], longPressCapital: [':'], keySize: 1},
              {key: 'n', capitalKey: 'N', longPress: [';'], longPressCapital: [';'], keySize: 1},
              {key: 'm', capitalKey: 'M', longPress: ['/'], longPressCapital: ['\\'], keySize: 1},
              {key: 'Backspace', capitalKey: 'Backspace', longPress: [], longPressCapital: [], keySize: 2},
            ],
            [
              {key: 'Close', capitalKey: 'Close', longPress: [], longPressCapital: [], keySize: 1},
              {key: ',', capitalKey: ',', longPress: [], longPressCapital: [], keySize: 1},
              {key: ' ', capitalKey: ' ', longPress: [], longPressCapital: [], keySize: 5},
              {key: '.', capitalKey: '.', longPress: ['!', '?'], longPressCapital: ['!', '?'], keySize: 1},
              {key: 'Enter', capitalKey: 'Enter', longPress: [], longPressCapital: [], keySize: 2},
            ]
          ];
          return new AdvancedKeyboard(options, callback, keys);
        }
      };

      var addLayout = function(name, keys, advanced = false) {
        layouts[name] = function(options, callback) {
          if(advanced) return new AdvancedKeyboard(options, callback, keys);
          return new Keyboard(options, callback, keys);
        };
      };

      var longestLine = function(keys) {
        var count = 0;
        keys.forEach((line, _) => {
          if(line.length > count) count = line.length;
        });
        return count;
      }

      var createBasicKeyboardContainer = function(options, keys) {
        var container = new createjs.Container();
        container.height = keys.length * options.keySize + (keys.length + 1) * options.keyMargin;
        var countLine = longestLine(keys);
        container.width =  countLine * options.keySize + (countLine + 1) * options.keyMargin;

        // background of keyboard
        var background = new createjs.Shape();
        background.graphics.beginFill("rgba(112, 109, 109, 0.5)").drawRect(0, 0, container.width, container.height);
        container.addChild(background);
        return container;
      }

      /**
       * var advancedKeyboard - description
       *
       * @param  {type} options  description
       * @param  {type} callback description
       * @param  {type} keys     two-dimensional array with objects e.g. {key: 'a', capitalKey: 'A', longPress: ['@', 'ä'], longPressCapital: ['@', 'Ä'], keySize: 1}
       * @return {type}          description
       */
      var AdvancedKeyboard = function(options, callback, keys) {
        var container = createBasicKeyboardContainer(options, keys);
        var shift = true;
        var keyObjs = [];
        // add keys
        var x;
        var y = options.keyMargin;
        keys.forEach((line, _) => {
          var objLine = [];
          x = options.keyMargin;
          line.forEach((key, _) => {
            if(key.key !== 'Empty') {
              var keyObject = MTLG.utils.uiElements.addButton({text: (shift ? key.capitalKey : key.key), sizeX: options.keySize * key.keySize + (key.keySize -1) * options.keyMargin, sizeY: options.keySize}, function() {
                // dummy because of press long functionality
              });
              var timeout = 500;
              var openPressMenu = function () {
                if((shift ? key.longPressCapital : key.longPress).length == 0) return;
                pressMenuOpen = true;
                // Open pressLongMenu
                var longPressKeys = (shift ? key.longPressCapital : key.longPress);
                longPressContainer = createBasicKeyboardContainer(options, [longPressKeys]);
                var longPressX = options.keyMargin;
                var longPressY = options.keyMargin;
                longPressKeys.forEach((longPressKey, _) => {
                  var longPressKeyObj = MTLG.utils.uiElements.addButton({text: longPressKey, sizeX: options.keySize, sizeY: options.keySize}, function() {
                    callback(longPressKey);
                    removeLongPressContainer();
                  });
                  longPressKeyObj.x = longPressX + options.keySize / 2;
                  longPressKeyObj.y = longPressY + options.keySize / 2;
                  longPressContainer.addChild(longPressKeyObj);
                  longPressX += options.keySize + options.keyMargin;
                });

                longPressContainer.x = keyObject.x;
                longPressContainer.y = keyObject.y;
                keyObject.parent.addChild(longPressContainer);
              }
              var timer = {
                start: function () {
                  if (typeof this.timeoutID === 'number') {
                    this.reset();
                  }
                  this.timeoutID = window.setTimeout(function() {
                    openPressMenu();
                  }, timeout);
                },
                reset: function () {
                  window.clearTimeout(this.timeoutID);
                }
              };
              var wasMoved = false;
              var pressMenuOpen = false;
              var longPressContainer;
              var removeLongPressContainer = function() {
                keyObject.parent.removeChild(longPressContainer);
                document.removeEventListener('click', removeLongPressContainer);
              }
              // for long press
              keyObject.on("mousedown", function() {
                wasMoved = false;
                pressMenuOpen = false;
                timer.start();
                return false;
              });
              keyObject.on("pressmove", function() {
                timer.reset();
                wasMoved = true;
              })
              keyObject.on("pressup", function() {
                timer.reset();
                if(!wasMoved && !pressMenuOpen) {
                  if(key.key === 'Shift') {
                    toggleShift();
                  } else {
                    callback((shift ? key.capitalKey : key.key));
                  }
                } else {
                  setTimeout(() => {document.addEventListener('click', removeLongPressContainer)}, 0);
                }
                return false;
              })

              keyObject.x = x + (options.keySize  * key.keySize + (key.keySize -1) * options.keyMargin)/2;
              keyObject.y = y + (options.keySize)/2;

              container.addChild(keyObject);
              key.obj = keyObject;
              objLine.push(key);
            }
            x += (options.keySize * key.keySize + (key.keySize -1) * options.keyMargin) + options.keyMargin;
          });
          y += options.keySize + options.keyMargin;
          keyObjs.push(objLine);
        });

        var toggleShift = function() {
          shift = !shift;
          keyObjs.forEach((line, _) => {
            line.forEach((key, _) => {
              key.obj.children[1].text = shift ?  key.capitalKey : key.key;
            });
          });
        }

        return container;
      }



      var Keyboard = function(options, callback, keys) {
        var container = createBasicKeyboardContainer(options, keys);

        // add keys
        var x;
        var y = options.keyMargin;
        keys.forEach((line, _) => {
          x = options.keyMargin;
          line.forEach((key, _) => {
            var keyObject = MTLG.utils.uiElements.addButton({text: key, sizeX: options.keySize, sizeY: options.keySize}, function() {
              callback(key);
            });
            keyObject.x = x + options.keySize/2;
            keyObject.y = y + options.keySize/2;
            // keyObject.regX = 0;
            // keyObject.regY = 0;
            x += options.keySize + options.keyMargin;
            container.addChild(keyObject);
          });
          y += options.keySize + options.keyMargin;
        });



        return container;
      }

      var createKeyboard = function(options, callback, layout) {
        var container = layouts[layout](options, callback); // creates the keyboard
        var stage = MTLG.getStageContainer();
        container.x = options.keyboardX;
        container.y = options.keyboardY;
        container.regX = container.width / 2;
        container.regY = container.height / 2;
        // make container movable
        if(options.movable) {
          container.on('pressmove', function(event) {
            var bounds = stage.globalToLocal(event.stageX, event.stageY);
            container.x = bounds.x;
            container.y = bounds.y;
          });
        }
        stage.addChild(container);
        return container;
      }

      var VirtualKeyboard = function(layout) {
        this.wasActivated = false;
        this.callback;
        this.inputfield;
        this.layoutName = layout;

        this.inputFieldOptions = {
          enterButton: true,
          enterPress: true,
          keySize: 50,
          keyMargin: 5,
          movable: true,
          keyboardX: null,
          keyboardY: null,
        }

        this.setLayout = (name) => {
          this.layoutName = name;
        }

        this.addLayout = addLayout;

        this.setCallback = (newCallback) => {
          this.callback = newCallback;
        }

        this.getInputFieldOptions = () => {
          return this.inputFieldOptions;
        }

        this.setInputFieldOptions = (options) => {
          this.inputFieldOptions = options;
        }

        this.getText = () => {
          return this.inputfield._text.text;
        }

        this.onKeyPress = (key) => {
          if(key == 'Backspace'){
            this.inputfield._text.text = this.inputfield._text.text.slice(0, -1);
          } else if(key == 'Enter'){
            if(this.inputFieldOptions.enterPress) this.callback(this.getText());
          } else if(key == 'Close'){
            MTLG.getStageContainer().removeChild(this.keyboard);
          } else {
            this.inputfield._text.text += key;
          }
        }


        this.onClickFunction = (container) => {
          console.log('Virtual Keyboard method on click', container);
          this.inputfield = container._inputfield;
          if(!this.layoutName) {
            console.error('No layout set');
            return;
          }
          if (!this.wasActivated) {
            this.wasActivated = true;
            this.inputfield._text.text = "";
          }
          if(this.inputFieldOptions.keyboardX === null) this.inputFieldOptions.keyboardX = container.x;
          if(this.inputFieldOptions.keyboardY === null) this.inputFieldOptions.keyboardY = container.y;

          if(!this.keyboard) {
            this.keyboard = createKeyboard(this.inputFieldOptions, this.onKeyPress, this.layoutName);
          } else {
            MTLG.getStageContainer().addChild(this.keyboard);
          }

        }
      }

      return {
        VirtualKeyboard: VirtualKeyboard
      }
    })();


    var DistributedDisplaysKeyboardController = (function() {
      var inputsForDistributedDisplay = {};
      var DistributedDisplayKeyboard = function() {
        this.callback;
        this.identifier;
        this.wasActivated = false;
        this.inputfield;
        this.container;

        this.inputFieldOptions = {
          enterButton: false,
          enterPress: false,
          enterWhenReceiving: true,
          qrcode: {
            width: 200,
            height: 200,
            url: "http://tabula-content.informatik.rwth-aachen.de/games/mtlg-input-utility/"
          },

        }

        this.initDistributedDisplays = function() {
          MTLG.distributedDisplays.actions.setCustomFunction("getInput", function(id, input) {
            inputsForDistributedDisplay[id].getInputFromDistributedDisplay(id, input);
            MTLG.distributedDisplays.communication.sendCustomAction("inputKeyboards","inputReceived", true);
          });
        }

        this.init = () => {
          this.identifier = Date.now();
          inputsForDistributedDisplay[this.identifier] = this;
          this.inputFieldOptions.qrcode.url += "?identifier=" + this.identifier;
          MTLG.distributedDisplays.rooms.createRoom('inputKeyboards', function(result) {
            if (result && result.success) {
              this.initDistributedDisplays();
            } else {
              // This client is a slave. Now it tries to join the room
              MTLG.distributedDisplays.rooms.joinRoom('inputKeyboards', function(result) {
                if (result && result.success) {
                  this.initDistributedDisplays();
                }
              }.bind(this));
            }
          }.bind(this));
        };
        //init
        this.init();



        this.getInputFromDistributedDisplay = function(id, input) {
          if (this.identifier == id) {
            this.inputfield._text.text = input;
            if(this.inputFieldOptions.enterWhenReceiving) this.callback(input);
          }
        }


        this.setCallback = (newCallback) => {
          this.callback = newCallback;
        }

        this.getInputFieldOptions = () => {
          return this.inputFieldOptions;
        }

        this.setInputFieldOptions = (options) => {
          this.inputFieldOptions = options;
        }

        this.onClickFunction = (con) => {
          console.log('DistributedDisplayKeyboard method on click', con);
          this.inputfield = con._inputfield;
          this.container = con;
          if (!this.wasActivated) {
            this.wasActivated = true;
            this.inputfield._text.text = "Scan QRCode";
          }
          // create and show qrCode
          this.showQRCode();
        }

        this.showQRCode = function() {
          MTLG.utils.qrcode.createQRCode(this.inputFieldOptions.qrcode.url, function(result) {
            if (result.success) {
              var stage = MTLG.getStageContainer();
              // get bounds for positioning
              var con = new createjs.Container();
              con.x = this.container.x;
              con.y = this.container.y + this.container.height;
              con.regX = (this.inputFieldOptions.qrcode.width) * (1 / 2);
              con.regY = (this.inputFieldOptions.qrcode.width) * (1 / 2);

              var close = MTLG.utils.uiElements.addButton({
                text: "x",
                sizeX: this.inputFieldOptions.qrcode.width / 10,
                sizeY: this.inputFieldOptions.qrcode.width / 10
              }, function() {
                stage.removeChild(con);
              });
              con.addChild(result.bitmap);
              con.addChild(close);

              con.on("pressmove", (event) => {
                con.set(stage.globalToLocal(event.stageX, event.stageY));
              });

              // add to stage
              stage.addChild(con);
            } else {
              console.log("Not working qrcode");
            }

          }.bind(this), {
            width: this.inputFieldOptions.qrcode.width
          });

        }
      }
      return {
        DistributedDisplayKeyboard: DistributedDisplayKeyboard
      }
    })();


    return {
      HardwareKeyboard: HardwareKeyboard,
      VirtualKeyboard: VirtualKeyboardController.VirtualKeyboard,
      DistributedDisplayKeyboard: DistributedDisplaysKeyboardController.DistributedDisplayKeyboard,
    }
  })();

  return {
    getInputField: Controller.getInputField,
    Methods: Methods,

    init: init
  };
})();

module.exports = inputKeyboards;
